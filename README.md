# petit panda

A low-level editor, one step above hexadecimal.

I wanted to know the meaning of every bit in a data stream.
(Eventually, craft timestamps)

For portability purpose, I use pure Python 3 and GTK+ 3 through PyGObject.

## Data streams

Data streams could be

 - small files on a local filesystem
 - huge files accessible over the network
 - the heap of a program being debugged
 - bytes in the MBR of a local disk

Thus we'll use an interface.

## Specifications

After reading some related studies (https://twitter.com/qolund/status/436069458675642368), I chosed to consider only the tree-like file formats.
We'll instanciate `metaspec` objects with a empty root node, and build a tree of mandatory, optional, variable-width, fields.

Those `metaspec` objects will act as `spec` generators, when given a data stream.
`spec` objects are simple trees describing the fields found in a given data stream.
They are not meant to be structurally modified.

## Fields

Fields are the leaves of our specification trees.
They should have :
 - a parent block
 - a relative offset
 - a type
 - value constraints


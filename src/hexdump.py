# coding: utf-8

from gi.repository import Gtk
from gi.repository import GLib

def safe_classic(byte):
  assert(byte % 256 == byte)
  if (byte < 0x20) or (byte > 0x7e):
    return '.'
  return GLib.markup_escape_text(chr(byte))

def safe_unicode(byte):
  assert(byte % 256 == byte)
  if (byte % 0x80 < 0x20) or byte == 0x7f or byte == 0xa0:
    return '.'
  return GLib.markup_escape_text(chr(byte))

safe_codepage = safe_unicode

def safe_blue(byte):
  assert(byte % 256 == byte)
  if byte < 0x80:
    if (byte < 0x20) or byte == 0x7f:
      return '.'
    else:
      return GLib.markup_escape_text(chr(byte))
  else:
    if (byte%0x80 < 0x20) or (byte%0x80 == 0x7f):
      z = '.'
    else:
      z = GLib.markup_escape_text(chr(byte%0x80))
    return "<span color='blue'>{}</span>".format(z)
    
def hexdump_mixed(byte):
  assert(byte % 256 == byte)
  if byte == 0x00:
    return "<span color='#ccc'>{:02x}</span>".format(byte)
  if byte < 0x21 or byte > 0x7e :
    return '{:02x}'.format(byte)
  color = 'blue'
  if byte in range(ord('a'),ord('z')+1): color = 'red'
  if byte in range(ord('A'),ord('Z')+1): color = 'orange'
  if byte in range(ord('0'),ord('9')+1): color = 'purple'
  
  return "<span color='{}'> {}</span>".format(color,GLib.markup_escape_text(chr(byte)))
  
class Hexdump:
  
  def __init__(self,data):
    self.hexdump_data_modes = ['byte','word','mixed','mixed unicode']
    self.ascii_data_modes = ['none','classic','blue','unicode','codepage850']

    self.line_width = 16
    self.widget = Gtk.ScrolledWindow()

    self.databox = Gtk.HBox()
    
    
    self.hexdump = Gtk.Label()
    self.asciidump = Gtk.Label()
    
    self.widget.add(self.databox)
    self.databox.add(self.hexdump)
    self.databox.add(self.asciidump)

    
    self.data = data
    self.range_start  = 0
    self.range_stop   = len(self.data)
    self.hexdump_data_mode = 'mixed'
    self.ascii_data_mode = 'none'
    self.update()
    self.window = None

  def update(self):

    # peut-être que le formatage du hexdump en une ligne n'est pas une bonne idée. Il faudra utiliser les données décrites par l'arbre
    # associer à chaque octet un attribut (une couleur, une référence vers une entité à laquelle il appartient peut être une bonne idée
    if self.hexdump_data_mode == 'mixed':
      markup = '<tt>{}</tt>'.format(
        '\n'.join([
          ''.join([
            hexdump_mixed(self.data[i+j])
            for j in range(self.line_width) if i+j<self.range_stop
            ])
          for i in range(self.range_start,self.range_stop,self.line_width)
          ]))
    elif self.hexdump_data_mode == 'byte':
      markup = '<tt>{}</tt>'.format(
        '\n'.join([
          ' '.join([
            '{:02x}'.format(self.data[i+j])
            for j in range(self.line_width) if i+j<self.range_stop
            ])
          for i in range(self.range_start,self.range_stop,self.line_width)
          ]))
    elif self.hexdump_data_mode == 'word':
      markup = '<tt>'+'{}'.format(
        '\n'.join([
          ' '.join([
            '{:02x}'.format(self.data[i+j])+
            '{:02x}'.format(self.data[i+j+1])
            for j in range(0,self.line_width,2) if i+j+1 < self.range_stop
            ])
          for i in range(self.range_start,self.range_stop,self.line_width)
          ]))
      if len(self.data ) % 2 == 1:
        markup += '' if len(self.data) % self.line_width == 1 else ' '
        markup += '{:02x}'.format(self.data[-1])
      markup += '</tt>'

    self.hexdump.set_markup(markup)

    
    if self.ascii_data_mode == 'none':
      markup = ''
    else:
      f = [None,safe_classic,safe_blue,safe_unicode,safe_codepage][self.ascii_data_modes.index(self.ascii_data_mode)]
      markup = '<tt>{}</tt>'.format(
        '\n'.join([
          ''.join([
            f(self.data[i+j])
            for j in range(self.line_width) if i+j<self.range_stop
            ])
          for i in range(self.range_start,self.range_stop,self.line_width)
          ]))
    
    self.asciidump.set_markup(markup)
    



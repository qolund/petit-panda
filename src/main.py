#!/usr/bin/env python3
# coding: utf-8

import sys
import hexdump
from gi.repository import Gtk

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print('Usage : {} file.dat'.format(sys.argv[0]))
  else:
    # instanciate window
    win = Gtk.Window()
    win.connect("destroy", Gtk.main_quit)
    win.connect("delete-event", Gtk.main_quit)
    win.set_title('Color gtk hexdump 👍')
    win.set_border_width(0)

    # open file
    with open(sys.argv[1],'rb') as ofi:
      # read data
      data = ofi.read()
      ofi.close()

    # create hexdump object
    hd = hexdump.Hexdump(data)
    hd.line_width = 32
    hd.update()

    # put it on the window
    win.add(hd.widget)

    # show it to the user
    win.show_all()
    Gtk.main()
